import React from "react";

export const themes = {
    uppercase: {
        textTransform: 'uppercase',
    },
    none: {
        textTransform: 'none',
    },
};

export const ThemeContext = React.createContext(['none', () => {}]);
