import React from "react";
import {QueryRenderer} from "react-relay";
import RelayEnvironment from "../RelayEnvironment";
import CityList from "./CityList";
import {CircularProgress} from "@material-ui/core";

const graphql = require('babel-plugin-relay/macro');

function CityListContainer({country}) {
    return (
        <QueryRenderer
            environment={RelayEnvironment}
            query={graphql`
                        query CityListContainerQuery($locationInput: LocationInput!){
                            country(input: $locationInput) {
                                cities {
                                    ...CityList_cities
                                }
                            }
                        }
                    `}
            variables={{
                locationInput: {slug: country},
            }}
            render={({error, props}) => {
                if (error) {
                    return <div>{error.message}</div>;
                }
                if (!props) {
                    return <CircularProgress />;
                }
                return (
                    <CityList cities={props.country.cities}/>
                );
            }}
        />
    );
}

export default CityListContainer;