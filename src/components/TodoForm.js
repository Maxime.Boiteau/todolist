import {React, useContext, useState} from "react";
import {ThemeContext, themes} from '../ThemeContext';

function TodoForm({saveTodo}) {
    const [value, setValue] = useState('');
    const [theme] = useContext(ThemeContext);
    const currentTheme = themes[theme];

    function validateForm() {
        if (value && value !== '') {
            saveTodo(value);
        }
    }

    return (
        <form onSubmit={(event => {
            event.preventDefault();
            validateForm();
        })}>
            <input
                placeholder="Add todo"
                onChange={(event) => setValue(event.target.value)}>
            </input>
            <br/>
            <button color="primary"
                    type="submit">
                <label style={{textTransform: currentTheme.textTransform}}>Add element</label>
            </button>
        </form>
    );
}

export default TodoForm;