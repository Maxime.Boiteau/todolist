import React, {useContext, useEffect, useState} from 'react';
import "./TodoScreen.css"
import TodoList from "./TodoList";
import TodoForm from "./TodoForm";
import ThemeToggler from "./ThemeToggler";
import {ThemeContext, themes} from "../ThemeContext";
import {Card} from "@material-ui/core";

function TodoScreen({changeColor}) {
    const [elements, setElements] = useState(["ElementB", "ElementC", "ElementZ", "ElementA", "ElementY"]);
    const [isSorted, setIsSorted] = useState(false);
    const [theme] = useContext(ThemeContext);
    const currentTheme = themes[theme];

    useEffect(() => {
        changeColor()
    }, [elements]);

    function saveTodo(element) {
        let newElements = [...elements];
        newElements.push(element);
        setElements(newElements);
    }

    function deleteTodo(element) {
        let newElements = [...elements];
        const index = elements.indexOf(element);
        newElements.splice(index, 1);
        setElements(newElements);
    }

    return (
        <Card className="TodoContainer">
            <h1>TodoList</h1>
            <br/>
            <TodoForm saveTodo={saveTodo}/>
            <br/>
            <TodoList elements={elements} deleteTodo={deleteTodo} isSorted={isSorted}/>
            <button className="SortButton"  onClick={() => {
                if (isSorted) {
                    setIsSorted(false);
                } else
                    setIsSorted(true);
            }
            }>
                <label style={{textTransform: currentTheme.textTransform}}> {isSorted ? 'Unsort' : 'Sort by alphabetical order'}</label>
            </button>
            <br/>
            <ThemeToggler/>
        </Card>
    );
}

export default TodoScreen;