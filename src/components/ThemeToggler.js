import {useContext} from "react";
import {ThemeContext, themes} from "../ThemeContext";

const ThemeToggler = () => {
    const [theme, setTheme] = useContext(ThemeContext);
    const currentTheme = themes[theme];
    return (
        <button onClick={() => setTheme(theme === "none" ? "uppercase" : "none")}>
            <label style={{textTransform: currentTheme.textTransform}}>
                Change theme
            </label>
        </button>
    );
}

export default ThemeToggler;