import React, {useState} from "react";
import './App.css';
import TodoScreen from "./TodoScreen";
import {ThemeContext} from "../ThemeContext";
import CountryListContainer from "./CountryListContainer";
import BgColorList from "../BgColorList";

function App() {
    const themeHook = useState("none");
    const [bgColorIndex, setBgColorIndex] = useState(0);

    const handleChangeColor = () => {
        setBgColorIndex(BgColorList.changeBackgroundColor(bgColorIndex));
    }

    return (
        <div className="App" style={{backgroundColor: BgColorList.bgColors[bgColorIndex]}}>
            <ThemeContext.Provider value={themeHook}>
                <TodoScreen changeColor={handleChangeColor} />
                <div className="CountryListContainer">
                    <CountryListContainer/>
                </div>
            </ThemeContext.Provider>
        </div>
    );
}

export default App;
