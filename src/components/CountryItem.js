import React, {useState} from "react";
import CityListContainer from "./CityListContainer";
import "./CountryItem.css"
import {Card, Collapse} from "@material-ui/core";

function CountryItem({name, slug}) {
    const [showCities, setShowCities] = useState(false);

    return (
        <Card onClick={() => setShowCities(!showCities ? true : true)} className="CountryCard">
            <h3>{name}</h3>
            <Collapse in={showCities}
                  {...(showCities ? { timeout: 500 } : {})}>
                <CityListContainer country={slug}/>
            </Collapse>
        </Card>
    );
}

export default CountryItem;