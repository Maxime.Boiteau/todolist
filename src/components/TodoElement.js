import {Button} from "@material-ui/core";
import ClearIcon from '@material-ui/icons/Clear';

function TodoElement({element, deleteTodo}) {
    return (
        <div>
            {element}
            <Button onClick={() => {
                deleteTodo(element);
            }}>
                <ClearIcon/>
            </Button>
            <div/>
        </div>
    );
}

export default TodoElement;