import {createFragmentContainer} from 'react-relay';
import React from "react";

const graphql = require('babel-plugin-relay/macro');

const CityList = ({cities}) => (
    cities.map((city) => <div key={city.id}>{city.name}</div>)
)

export default createFragmentContainer(CityList, {
    cities: graphql`
        fragment CityList_cities on City @relay(plural: true)  {
            name,
            id
        }
    `,
});