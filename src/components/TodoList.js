import TodoElement from "./TodoElement";

function TodoList({elements, deleteTodo, isSorted}) {

    let todos = elements.slice();
    if (isSorted)
        todos.sort((a, b) => (a.toLowerCase().localeCompare((b.toLowerCase()))));
    return (
        <div>
            {
                todos.map((element, index) => (
                    <TodoElement key={index} element={element} deleteTodo={deleteTodo}/>
                ))}
        </div>
    );
}

export default TodoList;