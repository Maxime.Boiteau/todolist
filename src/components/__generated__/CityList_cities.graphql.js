/**
 * @flow
 */

/* eslint-disable */

'use strict';

/*::
import type { ReaderFragment } from 'relay-runtime';
import type { FragmentReference } from "relay-runtime";
declare export opaque type CityList_cities$ref: FragmentReference;
declare export opaque type CityList_cities$fragmentType: CityList_cities$ref;
export type CityList_cities = $ReadOnlyArray<{|
  +name: string,
  +id: string,
  +$refType: CityList_cities$ref,
|}>;
export type CityList_cities$data = CityList_cities;
export type CityList_cities$key = $ReadOnlyArray<{
  +$data?: CityList_cities$data,
  +$fragmentRefs: CityList_cities$ref,
  ...
}>;
*/


const node/*: ReaderFragment*/ = {
  "argumentDefinitions": [],
  "kind": "Fragment",
  "metadata": {
    "plural": true
  },
  "name": "CityList_cities",
  "selections": [
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "name",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "id",
      "storageKey": null
    }
  ],
  "type": "City",
  "abstractKey": null
};
// prettier-ignore
(node/*: any*/).hash = 'f1754199e7c39f84fc558a722443d284';

module.exports = node;
