/**
 * @flow
 */

/* eslint-disable */

'use strict';

/*::
import type { ReaderFragment } from 'relay-runtime';
import type { FragmentReference } from "relay-runtime";
declare export opaque type CountryList_countries$ref: FragmentReference;
declare export opaque type CountryList_countries$fragmentType: CountryList_countries$ref;
export type CountryList_countries = $ReadOnlyArray<{|
  +name: string,
  +id: string,
  +slug: string,
  +$refType: CountryList_countries$ref,
|}>;
export type CountryList_countries$data = CountryList_countries;
export type CountryList_countries$key = $ReadOnlyArray<{
  +$data?: CountryList_countries$data,
  +$fragmentRefs: CountryList_countries$ref,
  ...
}>;
*/


const node/*: ReaderFragment*/ = {
  "argumentDefinitions": [],
  "kind": "Fragment",
  "metadata": {
    "plural": true
  },
  "name": "CountryList_countries",
  "selections": [
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "name",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "id",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "slug",
      "storageKey": null
    }
  ],
  "type": "Country",
  "abstractKey": null
};
// prettier-ignore
(node/*: any*/).hash = 'aa4d23b68f921d2e7bfe3ccd9e514ef7';

module.exports = node;
