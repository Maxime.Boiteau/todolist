/**
 * @flow
 */

/* eslint-disable */

'use strict';

/*::
import type { ConcreteRequest } from 'relay-runtime';
type CityList_cities$ref = any;
export type LocationInput = {|
  slug: string
|};
export type CityListContainerQueryVariables = {|
  locationInput: LocationInput
|};
export type CityListContainerQueryResponse = {|
  +country: {|
    +cities: ?$ReadOnlyArray<{|
      +$fragmentRefs: CityList_cities$ref
    |}>
  |}
|};
export type CityListContainerQuery = {|
  variables: CityListContainerQueryVariables,
  response: CityListContainerQueryResponse,
|};
*/


/*
query CityListContainerQuery(
  $locationInput: LocationInput!
) {
  country(input: $locationInput) {
    cities {
      ...CityList_cities
      id
    }
    id
  }
}

fragment CityList_cities on City {
  name
  id
}
*/

const node/*: ConcreteRequest*/ = (function(){
var v0 = [
  {
    "defaultValue": null,
    "kind": "LocalArgument",
    "name": "locationInput"
  }
],
v1 = [
  {
    "kind": "Variable",
    "name": "input",
    "variableName": "locationInput"
  }
],
v2 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "id",
  "storageKey": null
};
return {
  "fragment": {
    "argumentDefinitions": (v0/*: any*/),
    "kind": "Fragment",
    "metadata": null,
    "name": "CityListContainerQuery",
    "selections": [
      {
        "alias": null,
        "args": (v1/*: any*/),
        "concreteType": "Country",
        "kind": "LinkedField",
        "name": "country",
        "plural": false,
        "selections": [
          {
            "alias": null,
            "args": null,
            "concreteType": "City",
            "kind": "LinkedField",
            "name": "cities",
            "plural": true,
            "selections": [
              {
                "args": null,
                "kind": "FragmentSpread",
                "name": "CityList_cities"
              }
            ],
            "storageKey": null
          }
        ],
        "storageKey": null
      }
    ],
    "type": "Query",
    "abstractKey": null
  },
  "kind": "Request",
  "operation": {
    "argumentDefinitions": (v0/*: any*/),
    "kind": "Operation",
    "name": "CityListContainerQuery",
    "selections": [
      {
        "alias": null,
        "args": (v1/*: any*/),
        "concreteType": "Country",
        "kind": "LinkedField",
        "name": "country",
        "plural": false,
        "selections": [
          {
            "alias": null,
            "args": null,
            "concreteType": "City",
            "kind": "LinkedField",
            "name": "cities",
            "plural": true,
            "selections": [
              {
                "alias": null,
                "args": null,
                "kind": "ScalarField",
                "name": "name",
                "storageKey": null
              },
              (v2/*: any*/)
            ],
            "storageKey": null
          },
          (v2/*: any*/)
        ],
        "storageKey": null
      }
    ]
  },
  "params": {
    "cacheID": "f4a4651283d1b9cf0b3950f52d837579",
    "id": null,
    "metadata": {},
    "name": "CityListContainerQuery",
    "operationKind": "query",
    "text": "query CityListContainerQuery(\n  $locationInput: LocationInput!\n) {\n  country(input: $locationInput) {\n    cities {\n      ...CityList_cities\n      id\n    }\n    id\n  }\n}\n\nfragment CityList_cities on City {\n  name\n  id\n}\n"
  }
};
})();
// prettier-ignore
(node/*: any*/).hash = '06d343eee17172f71962ce6eaf915bd9';

module.exports = node;
