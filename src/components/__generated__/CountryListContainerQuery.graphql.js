/**
 * @flow
 */

/* eslint-disable */

'use strict';

/*::
import type { ConcreteRequest } from 'relay-runtime';
type CountryList_countries$ref = any;
export type CountryListContainerQueryVariables = {||};
export type CountryListContainerQueryResponse = {|
  +countries: $ReadOnlyArray<{|
    +$fragmentRefs: CountryList_countries$ref
  |}>
|};
export type CountryListContainerQuery = {|
  variables: CountryListContainerQueryVariables,
  response: CountryListContainerQueryResponse,
|};
*/


/*
query CountryListContainerQuery {
  countries {
    ...CountryList_countries
    id
  }
}

fragment CountryList_countries on Country {
  name
  id
  slug
}
*/

const node/*: ConcreteRequest*/ = {
  "fragment": {
    "argumentDefinitions": [],
    "kind": "Fragment",
    "metadata": null,
    "name": "CountryListContainerQuery",
    "selections": [
      {
        "alias": null,
        "args": null,
        "concreteType": "Country",
        "kind": "LinkedField",
        "name": "countries",
        "plural": true,
        "selections": [
          {
            "args": null,
            "kind": "FragmentSpread",
            "name": "CountryList_countries"
          }
        ],
        "storageKey": null
      }
    ],
    "type": "Query",
    "abstractKey": null
  },
  "kind": "Request",
  "operation": {
    "argumentDefinitions": [],
    "kind": "Operation",
    "name": "CountryListContainerQuery",
    "selections": [
      {
        "alias": null,
        "args": null,
        "concreteType": "Country",
        "kind": "LinkedField",
        "name": "countries",
        "plural": true,
        "selections": [
          {
            "alias": null,
            "args": null,
            "kind": "ScalarField",
            "name": "name",
            "storageKey": null
          },
          {
            "alias": null,
            "args": null,
            "kind": "ScalarField",
            "name": "id",
            "storageKey": null
          },
          {
            "alias": null,
            "args": null,
            "kind": "ScalarField",
            "name": "slug",
            "storageKey": null
          }
        ],
        "storageKey": null
      }
    ]
  },
  "params": {
    "cacheID": "2d14250262d511e2974fdc69972f62ae",
    "id": null,
    "metadata": {},
    "name": "CountryListContainerQuery",
    "operationKind": "query",
    "text": "query CountryListContainerQuery {\n  countries {\n    ...CountryList_countries\n    id\n  }\n}\n\nfragment CountryList_countries on Country {\n  name\n  id\n  slug\n}\n"
  }
};
// prettier-ignore
(node/*: any*/).hash = '7aa8ab2f5253aeecd5129beb5f173256';

module.exports = node;
