import {createFragmentContainer} from 'react-relay';
import React from "react";
import CountryItem from "./CountryItem";
import {Card} from "@material-ui/core";

const graphql = require('babel-plugin-relay/macro');

function CountryList({countries}) {
    return (
        (
            countries.map((country) =>
                <CountryItem key={country.id} name={country.name} slug={country.slug}/>
            )
        )
    );
}

export default createFragmentContainer(CountryList, {
    countries: graphql`
        fragment CountryList_countries on Country @relay(plural: true) {
            name,
            id,
            slug
        }
    `,
});