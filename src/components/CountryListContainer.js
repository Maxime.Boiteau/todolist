import React from "react";
import {QueryRenderer} from "react-relay";
import RelayEnvironment from "../RelayEnvironment";
import CountryList from "./CountryList";
import {CircularProgress, Grid} from "@material-ui/core";

const graphql = require('babel-plugin-relay/macro');

function CountryListContainer() {
    return (
        <QueryRenderer
            environment={RelayEnvironment}
            query={graphql`
                        query CountryListContainerQuery{
                            countries {
                               ...CountryList_countries
                            }
                        }
                    `}
            variables={{}}
            render={({error, props}) => {
                if (error) {
                    return <div>Error!</div>;
                }
                if (!props) {
                    return <CircularProgress/>;
                }
                return (
                    <Grid
                        container
                        direction="row"
                        justify="center"
                        alignItems="center"
                    >
                        <CountryList countries={props.countries}/>
                    </Grid>
                );
            }}
        />
    );
}

export default CountryListContainer;