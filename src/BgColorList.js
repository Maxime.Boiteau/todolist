class BgColorList {
    static bgColors = ['red', 'blue', 'yellow', 'green', 'purple'];

    static changeBackgroundColor(indexParam) {
        let index = indexParam;
        if (index === this.bgColors.length - 1)
            index = 0;
        else
            index = index + 1;
        return index;
    }
}

export default BgColorList;